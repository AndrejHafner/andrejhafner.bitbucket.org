
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var jimmyEhr = "8ff45429-9787-4c18-afe7-dab8b1ef5123";
var hendrixEhr = "5abc8e46-36dc-421f-969e-6e4367c6bbf9";
var janisEhr = "5e285141-e526-49aa-86ed-8c8a0f896ad0";
var cntLimit = 15;


function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


function generirajPodatke() {
    var date = new Date();
    kreirajEHRzaBolnika("Kekec","Kekcovic",new Date('2000-12-21T00:00:00Z').toISOString());
    kreirajEHRzaBolnika("Norma","Kokosec",new Date('1967-11-22T00:00:00Z').toISOString());
    kreirajEHRzaBolnika("Prepozno","Jedanes",new Date('2017-11-12T00:00:00Z').toISOString());

    alert("ID-ji generiranih bolnikov so v konzoli.");


}

function addMonthsUTC (date, count) {
    if (date && count) {
        var m, d = (date = new Date(+date)).getUTCDate()

        date.setUTCMonth(date.getUTCMonth() + count, 1)
        m = date.getUTCMonth()
        date.setUTCDate(d)
        if (date.getUTCMonth() !== m) date.setUTCDate(0)
    }
    return date
}

function kreirajEHRzaBolnika(name, lastname, dob) {
    sessionId = getSessionId();

    var ime = name;
    var priimek = lastname;
    var datumRojstva = dob;

    if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
        priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action === 'CREATE') {
                            console.log("Nov bolnik z IDjem: "+ehrId);
                            dodajMeritveVitalnihZnakov(ehrId,addMonthsUTC(new Date(),1).toISOString(),"Anita Klančar, dr. med.",0);

                        }
                    },
                    error: function(err) {

                    }
                });
            }
        });
    }
}

function dodajMeritveVitalnihZnakov(id,date,zdravnik,count) {
    sessionId = getSessionId();

    var ehrId = id;
    var datumInUra = date;
    var telesnaVisina = Math.floor(175+Math.random()*10);
    var telesnaTeza = Math.floor(75+Math.random()*20);
    var telesnaTemperatura = Math.floor(34+Math.random()*6);
    var sistolicniKrvniTlak = Math.floor(90+Math.random()*50);
    var diastolicniKrvniTlak = Math.floor(60+Math.random()*40);
    var nasicenostKrviSKisikom = Math.floor(90+Math.random()*10);
    var merilec = zdravnik;

    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
            committer: merilec
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                if(count<cntLimit)
                    dodajMeritveVitalnihZnakov(id,addMonthsUTC(new Date(),count).toISOString(),zdravnik,count+1);
            },
            error: function(err) {

            }
        });
    }
}