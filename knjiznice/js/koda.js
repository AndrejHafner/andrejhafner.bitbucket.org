
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var zdravnikiBaseUrl = "http://zdravniki.org";
var corsApiUrl = "https://cors-anywhere.herokuapp.com/";
var sessionId;
var doctors = [];
var avgTemp = 0;
var jimmyEhr = "8ff45429-9787-4c18-afe7-dab8b1ef5123";
var hendrixEhr = "5abc8e46-36dc-421f-969e-6e4367c6bbf9";
var janisEhr = "5e285141-e526-49aa-86ed-8c8a0f896ad0";

var trenutniBolnik = "";
var avgWeight = 0;
var niRezultatov = false;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function kreirajEHRzaBolnika() {
    sessionId = getSessionId();

    var ime = "Janis";
    var priimek = "Joplin";
    var datumRojstva = "1943-01-19T09:08";

    if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
        priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action === 'CREATE') {
                            $("#kreirajSporocilo").html("<span class='obvestilo " +
                                "label label-success fade-in'>Uspešno kreiran EHR '" +
                                ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    },
                    error: function(err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                            "label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            }
        });
    }
}

function preberiEHRodBolnika() {
    sessionId = getSessionId();

    var ehrId = $("#preberiEHRid").val();

    if (!ehrId || ehrId.trim().length == 0) {
        alert("Napacni ehrID!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                prikaziPodatkeBolnika(ehrId);
                return party;
            },
            error: function(err) {
                $("#preberiSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodat(stPacienta) {
  ehrId = "";



  return ehrId;
}





function getDoctors(specialization,city,page) {

    // setTimeout(function() {
    //     if($.isEmptyObject($("#seznamZdravnikov")))
    //         $("#seznamZdravnikov").append('<div style="text-align: center"><b>Ni rezultatov.</b><div>');
    // },2000);
    var response = $.ajax({
        type: "GET",
        headers: {'origin':''},
        url: corsApiUrl + zdravnikiBaseUrl + "/zdravniki?roles=0&specializations="+encodeURIComponent(specialization)+"&page="+page+"&cities="+encodeURIComponent(city),
        success:function(data) {
            var el = $('<div></div>');
            el.html(data);

            el = el.find('.listing');
            var doctorNames = el.find('.doctor');
            var doctorSpecs = el.find('.specialization');
            var doctorWorkplace = el.find('.workplace');

            for(var i = 0; i < doctorNames.length; i++) {
                var doctor = {name: doctorNames[i].text,
                              link: $(doctorNames[i]).attr('href'),
                              specs: doctorSpecs[i].innerText,
                              workplace: doctorWorkplace[i].innerText};
                // console.log(doctor);
                $("#seznamZdravnikov").append('<div class="col-md-12 doctor"> \
                    <h4><a class="doctor-name"  target="_blank" href="'+zdravnikiBaseUrl+'/'+doctor.link+'" id="imeZdravnika">'+doctor.name+'</a></h4> \
                    <a class="specialization">'+doctor.specs+'</a><br> \
                    <a class="workplace">'+doctor.workplace+'</a> \
                </div>');
                doctors.push(doctor);
            }
            if(doctorNames.length === 0 && doctors.length === 0) {
                $("#seznamZdravnikov").empty();
                $("#seznamZdravnikov").append('<div style="text-align: center"><b>Ni rezultatov.</b><div>');
            } else if(doctorNames.length !== 0) {
                getDoctors(specialization,city,page+1);
                console.log(doctors);
            }

        }
    });
}

function prikaziPodatkeOBolniku() {
    $("#dodatnoBolnik").html("" +
        "<button type='button' class='btn btn-default btn-xs' onclick='skrijPodatkeOBolniku()'>" +
        "<strong><span class='glyphicon glyphicon-arrow-up'></span>" +
        "<span class='naslov'>  Podatki o bolniku</strong></button><br>" +
        "<b>Povprečna teža:</b> " + avgWeight + " kg"+"<br>" +
        "<b>Povprečna telesna temperatura:</b> " + avgTemp + " °C"+"<br>"

    );
}

/**
 * Funkcija za master detail 2
 */
function skrijPodatkeOBolniku() {
    $("#dodatnoBolnik").html("" +
        "<button type='button' class='btn btn-default btn-xs' onclick='prikaziPodatkeOBolniku()'>" +
        "<strong><span class='glyphicon glyphicon-arrow-down'></span>" +
        "<span class='naslov'>  Podatki o bolniku</strong></button>"
    );
}



function preberiPodatkeBolnika() {
    var ehrId = $( "#izbiraBolnika option:selected" ).val() || jimmyEhr;
    sessionId = getSessionId();
    trenutniBolnik = ehrId;
    $("#imePacienta").html = "";
    $("#dobpacienta").html = "";
    $("#graf").empty();

    if (!ehrId || ehrId.trim().length == 0) {
        alert("Napacni ehrID!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#imePacienta").html(party.firstNames + " " + party.lastNames);
                var date = new Date(party.dateOfBirth);
                $("#dobPacienta").html(date.getDay()+"."+date.getMonth()+"."+date.getFullYear());
                $("#starostPacienta").html((2017-date.getFullYear())+" let");
                // $("#spolPacienta").html(party.gende)

                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/" + "weight",
                    type: 'GET',
                    headers: {"Ehr-Session": sessionId},
                    success: function (res) {
                        if (res.length > 0) {
                            var data = [];
                            var weightSum = 0;
                            res.forEach(function(each) {
                                data.push({date:each.time,
                                           weight:each.weight});
                                weightSum += each.weight;
                            });

                            avgWeight = Math.floor(weightSum / res.length);

                            new Morris.Line({
                                // ID of the element in which to draw the chart.
                                element: 'graf',
                                // Chart data records -- each entry in this array corresponds to a point on
                                // the chart.
                                data: data,
                                // The name of the data record attribute that contains x-values.
                                xkey: 'date',
                                // A list of names of data record attributes that contain y-values.
                                ykeys: ['weight'],
                                // Labels for the ykeys -- will be displayed when you hover over the
                                // chart.
                                labels: ['Value']
                            });
                        }
                    },
                    error: function() {

                    }
                });
            },
            error: function(err) {

            }
        });
    }

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
            if (res.length > 0) {
                var sumTemp = 0;
                res.forEach(function(each) {
                    sumTemp += each.temperature;
                });

                avgTemp = (sumTemp / res.length);

            } else {

            }
        },
        error: function() {

        }
    });

}

function prikaziPodatkeBolnika(id) {
    var ehrId = id;
    sessionId = getSessionId();
    trenutniBolnik = ehrId;
    $("#imePacienta").html = "";
    $("#dobpacienta").html = "";
    $("#graf").empty();

    if (!ehrId || ehrId.trim().length == 0) {
        alert("Napacni ehrID!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#imePacienta").html(party.firstNames + " " + party.lastNames);
                var date = new Date(party.dateOfBirth);
                $("#dobPacienta").html(date.getDay()+"."+date.getMonth()+"."+date.getFullYear());
                $("#starostPacienta").html((2017-date.getFullYear())+" let");
                // $("#spolPacienta").html(party.gende)

                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/" + "weight",
                    type: 'GET',
                    headers: {"Ehr-Session": sessionId},
                    success: function (res) {
                        if (res.length > 0) {
                            var data = [];
                            var weightSum = 0;
                            res.forEach(function(each) {
                                data.push({date:each.time,
                                    weight:each.weight});
                                weightSum += each.weight;
                            });
                            console.log(data);

                            avgWeight = Math.floor(weightSum / res.length);

                            new Morris.Line({
                                // ID of the element in which to draw the chart.
                                element: 'graf',
                                // Chart data records -- each entry in this array corresponds to a point on
                                // the chart.
                                data: data,
                                // The name of the data record attribute that contains x-values.
                                xkey: 'date',
                                // A list of names of data record attributes that contain y-values.
                                ykeys: ['weight'],
                                // Labels for the ykeys -- will be displayed when you hover over the
                                // chart.
                                labels: ['Value']
                            });
                        }
                    },
                    error: function() {

                    }
                });
            },
            error: function(err) {
                $("#preberiSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
            if (res.length > 0) {
                console.log(res);
                var sumTemp = 0;
                res.forEach(function(each) {
                    sumTemp += each.temperature;
                });

                avgTemp = (sumTemp / res.length);

            } else {

            }
        },
        error: function() {

        }
    });

}




function dodajMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = jimmyEhr;
    var datumInUra = "2017-10-29T09:00";
    var telesnaVisina = Math.floor(175+Math.random()*10);
    var telesnaTeza = Math.floor(75+Math.random()*20);
    var telesnaTemperatura = Math.floor(34+Math.random()*6);
    var sistolicniKrvniTlak = Math.floor(90+Math.random()*50);
    var diastolicniKrvniTlak = Math.floor(60+Math.random()*40);
    var nasicenostKrviSKisikom = Math.floor(90+Math.random()*10);
    var merilec = "Tomi Bremec, dr. med.";

    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
            committer: merilec
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-success fade-in'>" +
                    res.meta.href + ".</span>");
            },
            error: function(err) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}

function preberiTelesnoTemperaturo() {
    sessionId = getSessionId();

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
            console.log(res);
        },
        error: function() {

        }
    });
}

function preberiMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = jimmyEhr;
    var tip = 'telesna teža';

    if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                    "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                    " " + party.lastNames + "'</b>.</span><br/><br/>");
                if (tip == "telesna temperatura") {
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna temperatura</th></tr>";
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].temperature +
                                        " " + res[i].unit + "</td>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                } else if (tip == "telesna teža") {
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "weight",
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna teža</th></tr>";
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].weight + " " 	+
                                        res[i].unit + "</td>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                } else if (tip == "telesna temperatura AQL") {
                    var AQL =
                        "select " +
                        "t/data[at0002]/events[at0003]/time/value as cas, " +
                        "t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
                        "t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
                        "from EHR e[e/ehr_id/value='" + ehrId + "'] " +
                        "contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
                        "where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
                        "order by t/data[at0002]/events[at0003]/time/value desc " +
                        "limit 10";
                    $.ajax({
                        url: baseUrl + "/query?" + $.param({"aql": AQL}),
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            var results = "<table class='table table-striped table-hover'>" +
                                "<tr><th>Datum in ura</th><th class='text-right'>" +
                                "Telesna temperatura</th></tr>";
                            if (res) {
                                var rows = res.resultSet;
                                for (var i in rows) {
                                    results += "<tr><td>" + rows[i].cas +
                                        "</td><td class='text-right'>" +
                                        rows[i].temperatura_vrednost + " " 	+
                                        rows[i].temperatura_enota + "</td>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }

                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                }
            },
            error: function(err) {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}

function findDoctors() {
    var spec = $( "#specSelection option:selected" ).val();
    var city = $( "#citiesSelection option:selected" ).val();
    $("#seznamZdravnikov").empty();
    doctors = [];
    getDoctors(spec,city,1);
}

$(document).ready(function () {


    prikaziPodatkeBolnika(jimmyEhr);

})


